""" migration to increase the point_balance for user 2 to 1000, and the tier for user 3 to Bronze

Revision ID: 0000001
Revises: 0000000
Create Date: 2017-09-04 16:45:45

"""

# revision identifiers, used by Alembic.
revision = '00000001'
down_revision = '00000000'

from alembic import op
    
def upgrade():
    op.execute(
            '''UPDATE user 
            SET point_balance=1000 
            WHERE user_id=2'''
            )
            
    op.execute(
            '''UPDATE user 
            SET tier='Bronze' 
            WHERE user_id=3'''
            )

def downgrade():
    op.execute(
            '''UPDATE user 
            SET point_balance=0 
            WHERE user_id=2'''
            )
            
    op.execute(
            '''UPDATE user 
            SET tier='Carbon' 
            WHERE user_id=3'''
            )
