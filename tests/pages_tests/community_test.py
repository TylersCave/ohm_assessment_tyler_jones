from app_main import app
from tests import OhmTestCase

class CommunityTest(OhmTestCase):
    def get_five_most_recent_users(self):
        with app.test_client() as c:
            response = c.get('/community')
            assert "Justin Bieber" in response.data
            assert "Elvis Presley" in response.data
            assert "Chuck Norris" in response.data
            assert "Bronze" in response.data
            assert "1000" in response.data

